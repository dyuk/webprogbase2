$('button.remove').click( () => {
    console.log('sdf');
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  animation: false,
  customClass: 'animated rollIn',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
})
.then(() => {
    return new Promise(function(resolve,reject){resolve($.ajax({
       url: "/cities/",
       type: "POST",
       data: {
           name: $('button.remove').attr("name")
       }
    }))
    })
})
.then(data => {
    if (data) {
        swal(
            'Deleted!',
            'This page has been deleted.',
            'success'
        )
        .then(()=> {window.location.href = "/#cities";})
    }
    else {
        swal(
            'Ooops...',
            'Something went wrong.',
            'error'
        ) 
        .then(()=> {window.location.href = "/#cities";})
    }
})
})


