let express = require('express');
let bodyParser = require('body-parser');
let base64Img = require('base64-img');
const cities = require('./routes/cities');
const api = require('./routes/api');
const auth = require('./modules/auth');
let cloudinary = require('cloudinary');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const multer  = require('multer');
const Base64 = require('js-base64').Base64;
const ExifImage = require('exif').ExifImage;
const upload = multer({storage:multer.memoryStorage()});
const LocalStrategy = require('passport-local').Strategy;
mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/CitiesDB', { useMongoClient: true })
mongoose.connect('mongodb://Dyuk:Lthtdjyjvth1@ds251435.mlab.com:51435/heroku_8vczxmsl', { useMongoClient: true })
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));
let app = express();
let model = require('./modules/model');
cloudinary.config({ 
  cloud_name: 'triphelper', 
  api_key: '866391184439149', 
  api_secret: '3a0_sjSryM_XxSkb6Vs1BNkBrPU' 
});


app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(cookieParser());
app.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use("/cities", cities);
const PORT = process.env.PORT || 8090;

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    model.User.findOne({_id: id})
        .then(user => {
            if (!user) done("No user");
            else done(null, user);
        });
});

const serverSalt = "45%sAlT_";
function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

passport.use(new LocalStrategy(
    function (username, password, done) {
        let passwordHash = sha512(password, serverSalt).passwordHash;
        model.User.findOne({'username': username, 'passwordHash' : passwordHash})
            .then(user => {
                if (!user) return done("No user", false);
                return done(null, user);
            });
    }
));


app.get('/',
async (req,res) => {
	try {
		let data = await model.City.find();
		res.render('index', {data,user: req.user});
	} catch (e) {
		res.render('error', {});
	}
});

app.get('/users/profile',
    auth.checkAuth,
    async (req,res) => {
        try {
            res.render('profile', {user: req.user});
        } catch (e) {
            res.render('error', {});
        }
    })

app.post('/users/updateAvatar',
    auth.checkAuth,
    async (req, res) => {
        console.log(req.user.username);
        try {
            let img = req.body.image;
            let url = await cloudinary.uploader.upload(img);
            await model.User.update({'username': req.user.username},
            {
                image:url.url,
                username:"Admin"
            });
            res.send("OK");
        } catch (e) {
            res.send(null);
        }
    });

app.post('/searchMap',
    auth.checkAuth,
    async (req, res) => {
        try {
            let tempHistory = req.body;
            console.log(tempHistory);
            tempHistory.city = tempHistory.city.split(",")[0];
            let history = new model.History({
                city:tempHistory.city,
                people:tempHistory.people,
                days:tempHistory.days,
                price:0,
                user_id:req.user._id,
                lat:tempHistory.lat,
                lng:tempHistory.lng
                });
            history.save()
                .then(() =>  res.send("OK"))
                .catch((err) => {res.send(err.toString())});
        } catch (e) {
            res.send(e.toString());
        }
    });

app.post('/users/postImages',
    auth.checkAuth,
    upload.any(),
    async (req,res) => {
        try {
            for (let i = 0; i < req.files.length; i++) {
                let url = await cloudinary.uploader.upload('data:image/png;base64,'+ req.files[i].buffer.toString('base64'));
                new ExifImage({ image : req.files[i].buffer }, function (error, exifData) {
                    if (error)
                        console.log('Error: '+error.message);
                    else
                        console.log(exifData.gps); // Do something with your data!
                });
                let newUrl = url.url;
                let photo = new model.Photo({
                    image:newUrl,
                    history_id:req.body.history_id
                });
                photo.save();
            }
            res.send("success");
        } catch (e) {
            res.send("error");
        }
    })

app.get('/logout',
    (req, res) => {
        req.logout();
        res.redirect('/');
    });

app.get('/admin',
    auth.checkAdmin,
    (req, res) => res.render('admin', {
        user: req.user
    }));

app.post('/login',
    passport.authenticate('local'));

app.post('/register',
    async (req, res) => {
        let username = req.body.username;
        let password = req.body.password;
        let passwordHash = sha512(password, serverSalt).passwordHash;
        let user = {
            username,
            passwordHash,
            role: "user"
        };
        try {
            let userToSave = new model.User(user);
		    await userToSave.save();
            res.send("registered");
        } catch (e) {
            res.sendStatus(500);
        }
    });

app.get('/search',
async (req,res) => {
	try {
		let data = await model.City.find();
		res.send(data);
	} catch (e) {
		res.render('error', {});
	}
});

app.get('/histories',
    auth.checkAuth,
    async (req,res) => {
        try {
            let data = await model.History.find({user_id:req.user._id});
            res.status(200).json(data);
        } catch (e) {
            res.render('error', {});
        }
    });

app.get('/photos',
    auth.checkAuth,
    async (req,res) => {
        try {
            let id = req.body;
            let data = await model.History.find({history_id:id});
            res.status(200).json(data);
        } catch (e) {
            res.render('error', {});
        }
    });

app.get('/add',
auth.checkAdmin,
async (req,res) => {
	try {
		let data = await model.City.find();
		res.render('form', {data,user: req.user});
	} catch (e) {
		res.render('error', {});
	}
})

app.post('/add',
	async (req, res) => {
		try {
		let tempObject = req.body;
		tempObject.name=tempObject.name.replace(/\s/g, "_");
		let url = await cloudinary.uploader.upload(tempObject.image);
		tempObject.image = url.url;
		let tempCity = new model.City(tempObject);
		tempCity.save()
			.then(() =>  res.send(tempObject))
  			.catch((err) => {res.send(null)});
	} catch (e) {
		res.render('error', {});
	}   
});

app.use((req, res) => {
	res.render('error', {});
});


app.listen(PORT, () => {
  console.log('Server listening on: http://localhost:%s', PORT);
});

