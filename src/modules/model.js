const mongoose = require('mongoose');
let citySchema = mongoose.Schema({
    city: {type:String,unique : true,required:true},
	description: {type:String,required:true},
	name: {type:String,unique : true,required:true},
	image: {type:String,required:true},
	views: Number,
	likes: Number
});

let userSchema = mongoose.Schema({
	username: {type:String,unique : true,required:true},
	passwordHash: {type:String,required:true},
	role: {type:String,required:true},
	image: String
});

let historySchema = mongoose.Schema({
    user_id: mongoose.Schema.Types.ObjectId,
    people: Number,
    days: Number,
    city: String,
    price: Number,
    lat: Number,
    lng: Number
});

let photoSchema = mongoose.Schema({
    history_id: mongoose.Schema.Types.ObjectId,
    image:String
});

let City = mongoose.model('City', citySchema);
let User = mongoose.model('User', userSchema);
let History = mongoose.model('History', historySchema);
let Photo = mongoose.model('Photo', photoSchema);

module.exports = {
    City,
	User,
    History,
    Photo
};