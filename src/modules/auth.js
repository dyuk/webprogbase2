module.exports = {
checkAdmin: function(req, res, next) {
    if (!req.user) return res.render('error', {});
    if (req.user.role !== 'admin') return res.render('error', {});
    next();
},
checkAuth: function(req, res, next) {
    if (!req.user) return res.render('error', {});
    next();
}
}