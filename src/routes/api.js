let express = require('express');
let router = express.Router();
let model = require('.././modules/model');
const basicAuth = require('express-basic-auth');
const crypto = require('crypto');
let cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: 'triphelper', 
  api_key: '866391184439149', 
  api_secret: '3a0_sjSryM_XxSkb6Vs1BNkBrPU' 
});
router.use(express.static('public'));

const serverSalt = "45%sAlT_";
function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

var asyncAuth = basicAuth({
    authorizer: myAsyncAuthorizer,
    authorizeAsync: true,
    challenge: true,
    unauthorizedResponse: {success:false,error:"Authorization failed."}
})

async function myAsyncAuthorizer(username, password, cb) {
    const passwordHash = sha512(password, serverSalt).passwordHash;
    let user = await model.User.findOne({'username': username, 'passwordHash' : passwordHash});
    if (user)
        return cb(null, true)
    else
        return cb(null, false)
}

router.get('/all', 
async (req,res) => {
	try {
        let page = req.query.page || 0;
        let resObj;
        let allLength = await model.City.find().count();
        if (isNaN(page)) {
            res.json({success:false,error:"no such page"});
            return;
        }
        if (!page || page == 1) {
		    let data = await model.City.find().limit(5);
            if (page != Math.ceil(allLength/5)) {
                resObj = Object.assign({success:true},data,{page:`1 of ${Math.ceil(allLength/5)}`,nextPage:"http://localhost:8090/api/v1/cities/all?page=2"});
            } else {
                resObj = Object.assign({success:true},data,{page:`1 of ${Math.ceil(allLength/5)}`});
            }
        } else {
            let data = await model.City.find().skip(5*(page-1)).limit(5);
            let pagesObj;
            if (data.length != 0) {
                if (page != Math.ceil(allLength/5)) {
                    pagesObj = {page:`${page} of ${Math.ceil(allLength/5)}`,nextPage:`http://localhost:8090/api/v1/cities/all?page=${++page}`};
                } else {
                    pagesObj = {page:`${page} of ${Math.ceil(allLength/5)}`};
                }
                resObj = Object.assign({success:true}, data, pagesObj);
            }
            else resObj = {success:false,error:"no such page"};
        }
        res.json(resObj);
	} catch (e) {
		res.json({success:false,error:e.toString()});
	}
});

router.get('/:city_name(\[_a-zA-Z\s]+)',
async (req,res) => {
	try {
		let dataOne = await model.City.findOne({'name': req.params.city_name});
		if (dataOne )res.json(dataOne);
        else res.json({success:false,error:"No such element in database"});
	} catch (e) {
		res.json({success:false,error:e.toString()});
	}
});

router.delete('/delete', asyncAuth,
async (req,res) => {
    try {
        let data = await model.City.findOneAndRemove({'name': req.body.name});
        if (data) {
            let imgName = (data.image.match(/[\/][a-zA-Z0-9]+\.(png|jpg|jpeg|gif|svg)$/))[0];
            let publicId = imgName.match(/[a-zA-Z0-9]+/)[0];
            await cloudinary.uploader.destroy(publicId);
            res.json(data);
        }
        else res.json({success:false,error:"No element with such name in database"});
    } catch (e) {
        res.json(e.toString());
    }
});

router.post('/create', asyncAuth,
async (req, res) => {
    try {
        let tempObject = req.body;
        tempObject = Object.assign(tempObject, {views:0,likes:0});
        try {
            tempObject.name=tempObject.name.replace(/\s/g, "_");
            let url = await cloudinary.uploader.upload(tempObject.image);
            tempObject.image = url.url;
        } catch (e) {}
        let tempCity = new model.City(tempObject);
        tempCity.save()
            .then(() =>  res.json(tempObject))
            .catch((err) => res.json({success:false,error:"Some of required fields couldnt be found or are not unique"}));
	} catch (e) {
		res.json(e.toString());
	}
});

// function checkFields(obj) {
//     if (typeof(obj.name) === 'undefined'
//         || typeof(obj.image) === 'undefined'
//         || typeof(obj.city) === 'undefined'
//         || typeof(obj.description) === 'undefined') {return false;}
//     return true;
// }

module.exports = router;