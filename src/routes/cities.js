let express = require('express');
let router = express.Router();
let cloudinary = require('cloudinary');
let model = require('.././modules/model');
const auth = require('./../modules/auth');
cloudinary.config({ 
  cloud_name: 'triphelper', 
  api_key: '866391184439149', 
  api_secret: '3a0_sjSryM_XxSkb6Vs1BNkBrPU' 
});
router.use(express.static('public'));

router.get('/:city_name(\[_a-zA-Z\s]+)',
async (req,res) => {
	try {
		let data = await model.City.find();
		let dataOne = await model.City.findOne({'name': req.params.city_name});
		res.render('city', {dataOne,data,user: req.user});
	} catch (e) {
		res.render('error', {});
	}
});

router.post('/', 
auth.checkAdmin,
	async (req, res) => {
        try {
            let data = await model.City.findOneAndRemove({'name': req.body.name});
            res.send(data);
        } catch (e) {
            res.send(null);
	    }
    });

router.post('/update',
auth.checkAdmin,
	async (req, res) => {
		try {
		let tempObject = req.body;
		tempObject.name=tempObject.name.replace(/\s/g, "_");
		if (tempObject.image){
			console.log("is image");
			 let url = await cloudinary.uploader.upload(tempObject.image);
			 tempObject.image = url.url;
			 await model.City.update({'city': tempObject.prevCity},
			 {	
				city:tempObject.city,
				description:tempObject.description,
				name:tempObject.name,
				image:tempObject.image
			 }
			 );
		} else {
			await model.City.update({'city': tempObject.prevCity,},
			{	city:tempObject.city,
				description:tempObject.description,
				name:tempObject.name
			});
		}
	} catch (e) {
		res.render('error', {});
	}   
});

module.exports = router;