'use strict';

var loop = 0;
$(document).ready(function () {
  var scrollOverflow = true;
  if ($(window).width() < 768) {
    scrollOverflow = false;
  } else {
    scrollOverflow = true;
  }
  $('#fullpage').fullpage({
    anchors: ['main', 'about', 'popular', 'cities', 'map'],
    scrollOverflow: scrollOverflow,
    navigation: true,
    responsive: 768,
    navigationPosition: 'right',
    navigationTooltips: ['Main', 'About', 'Popular places', 'Cities', 'Map'],
    afterLoad: function afterLoad(anchorLink, index) {
      var loadedSection = $(this);
      if (index == 1) {
        $('.mark').addClass('animated bounceInDown');
        $('.right').find('h1').addClass('animated fadeInRight');
        $('.right').find('h3').addClass('animated fadeInRight');
        $('.right').find('ul').find('li').addClass('animated fadeInRight');
      }
    },
    onLeave: function onLeave(index, nextIndex, direction) {
      var loadedSection = $(this);
      if (index == 1 && direction == 'down') {
        loop++;
        $('.leftText').find('h1').addClass('animated fadeInDown');
        $('.leftText').find('hr').addClass('animated fadeIn');
        //$('.leftText').find('p').addClass('animated fadeInDown');
        $('.leftText').find('nav').find('a').addClass('animated fadeIn');
        if (loop == 1) {
          var typed = new Typed('.typed', {
            strings: ["my awesome site.", "site, where you can choose your place to go,^1000 learn about places you can visit near you point,^1000 prices in hotels,^1000 where to eat,^1000 look for other's trips etc."],
            typeSpeed: 30,
            backSpeed: 20,
            backDelay: 2500,
            startDelay: 1200,
            loop: false
          });
        }
      }
      if (index == 2 && direction == 'down') {
        $('.img-h3').find('h3').addClass('animated fadeInDown');
        $('.img-h3').find('ul').addClass('animated fadeInDown');
      }
    }
  });

  $('.mouseScroll').click(function (e) {
    e.preventDefault();
    $.fn.fullpage.moveSectionDown();
  });
  $('.moveSection').find('a').click(function (e) {
    e.preventDefault();
    $.fn.fullpage.moveSectionDown();
  });
  $('.mouseScrollUp').click(function (e) {
    e.preventDefault();
    $.fn.fullpage.moveSectionUp();
  });
});

//sliders
$(function () {
  var valtooltip = function valtooltip(sliderObj, ui) {
    var val1 = '<span class="slider-tip">' + sliderObj.slider("values", 0) + '</span>';
    var val2 = '<span class="slider-tip">' + sliderObj.slider("values", 1) + '</span>';
    sliderObj.find('.ui-slider-handle:first').html(val1);
    sliderObj.find('.ui-slider-handle:last').html(val2);
  };

  $("#slider-range2").slider({
    range: true,
    animate: true,
    min: 0,
    max: 5000,
    values: [2000, 5000],
    create: function create(event, ui) {
      valtooltip($(this), ui);
    },
    slide: function slide(event, ui) {
      valtooltip($(this), ui);
      $("#amountx").val("$" + ui.values[0] + " - $" + ui.values[1]);
    },
    stop: function stop(event, ui) {
      valtooltip($(this), ui);
    }
  });

  $("#amountx").val("$" + $("#slider-range2").slider("values", 0) + " - $" + $("#slider-range2").slider("values", 1));
  var initialSpark = 60;
  var sparkTooltip = function sparkTooltip(event, ui) {
    var curSpark = ui.value || initialSpark;
    var sparktip = '<span class="slider-tip">' + curSpark + '</span>';
    $(this).find('.ui-slider-handle').html(sparktip);
  };

  var months = ["1", "2", "3", "4", "5", "5+"];
  var $slidern = $("#slider2").slider({
    range: "min",
    animate: true,
    min: 0,
    max: 5,
    value: 1
  });

  $slidern.slider("pips", { rest: "label", labels: months });
  $slidern.slider("float", { labels: months });
  $slidern.on("slidechange", function (e, ui) {
    $("#monthOutput").html("Number of people: <span class=\"orangeText\">" + months[ui.value] + "</span>");
  });
});

var app = new Vue({
  el: "#header",
  data: {
    search: '',
    cities: [],
    page: 0,
    lastSearch: '',
    showModal: false
  },
  computed: {
    filteredCities: function filteredCities() {
      var self = this;
      if (self.search !== self.lastSearch) self.page = 0;
      self.lastSearch = self.search;
      var arr = _.chunk(this.cities.filter(function (cust) {
        if (self.search === "") return false;else {
          return (cust.name + cust.description).toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
        }
      }), 2);
      return {
        arr: arr[self.page],
        length: arr.length };
    },
    computedSearch: function computedSearch() {
      var self = this;
      return new RegExp(self.search, 'gi');
    }
  },
  mounted: function mounted() {
    var self = this;
    this.$http.get('/search').then(function (response) {
      console.log(response.body);
      self.cities = response.body;
    }, function (error) {
      console.log(error);
    });
  },

  methods: {
    changePage: function changePage(page) {
      this.page = page;
    }
  }
});

// modal

Vue.component('modal', {
  template: '\n        <transition name="modal">\n<div class="modal-mask">\n      <div class="modal-wrapper">\n        <div class="modal-container">\n\n          <div class="modal-header">\n            <slot name="header">\n              {{headerText}}\n            </slot>\n          </div>\n\n          <div class="modal-body">\n            <slot name="body">\n              <div class="login" v-show="login">\n                <label>Login</label>\n              <div class="input-group">\n                <div class="input-group-addon"><i class="fa fa fa-user" aria-hidden="true"></i></div>\n                <input type="text" class="form-control" v-bind:class="{\'is-invalid\': !username}" placeholder="Login" v-model="username">\n                <div class="invalid-feedback">Login is required!</div>\n              </div>\n                <label>Password</label>\n              <div class="input-group">\n                <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>\n                <input type="password" class="form-control" v-bind:class="{\'is-invalid\': !password}" placeholder="Password" v-model="password">\n                <div class="invalid-feedback">Password is required!</div>\n              </div>\n              <a href="#">Forgot password?</a>\n              <a href="#" @click="register=true,login=false,headerText=\'Sign in\',url=\'/register\',type=\'Registration\',msg=\'This login is not available.\'">Don\'t have account? Sign in</a>\n              </div>\n              <div class="register" v-show="register">\n                <label>Create login</label>\n              <div class="input-group">\n                <div class="input-group-addon"><i class="fa fa fa-user" aria-hidden="true"></i></div>\n                <input type="text" class="form-control" v-bind:class="{\'is-invalid\': !usernameReg}" placeholder="Create login" v-model="usernameReg">\n                <div class="invalid-feedback">Login is required!</div>\n              </div>\n                <label>Create password</label>\n              <div class="input-group">\n                <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>\n                <input type="password" class="form-control" v-bind:class="{\'is-invalid\': !passwordReg}" placeholder="Create password" v-model="passwordReg">\n                <div class="invalid-feedback">Password is required!</div>\n              </div>\n              <label>Repeat password</label>\n              <div class="input-group">\n                <div class="input-group-addon"><i class="fa fa-refresh" aria-hidden="true"></i></div>\n                <input type="password" class="form-control" v-bind:class="{\'is-invalid\': passwordReg!==passwordRegRepeat || !passwordRegRepeat}" placeholder="Repeat password" v-model="passwordRegRepeat">\n                <div class="invalid-feedback">Password is required!</div>\n                <div class="invalid-feedback" v-if="passwordReg!==passwordRegRepeat">Passwords are not equal</div>\n              </div>\n              </div>\n            </slot>\n          </div>\n\n          <div class="modal-footer">\n            <slot name="footer">\n              <button class="modal-default-button" @click="auth()">\n                OK\n              </button>\n            </slot>\n          </div>\n        </div>\n      </div>\n    </div>\n  </transition>\n    ',
  data: function data() {
    return {
      username: '',
      password: '',
      register: false,
      login: true,
      headerText: 'Log in',
      usernameReg: '',
      passwordReg: '',
      passwordRegRepeat: '',
      url: '/login',
      type: 'Authentication',
      msg: 'Login or password is invalid.'
    };
  },
  methods: {
    auth: async function auth() {
      if (this.usernameReg && this.passwordReg && this.passwordRegRepeat && this.passwordReg === this.passwordRegRepeat || this.type === 'Authentication' && this.password && this.username) {
        try {
          var _vm = this;
          var data = !this.usernameReg ? {
            username: this.username,
            password: this.password
          } : {
            username: this.usernameReg,
            password: this.passwordReg
          };
          var res = await _vm.$http.post(_vm.url, data);
          swal('Success!', this.type + ' successful.', 'success').then(function () {
            _vm.$emit('close');
            location.reload();
          });
        } catch (e) {
          swal('Ooops...', this.type + ' failed! ' + this.msg, 'error').then(function () {
            vm.$emit('close');
            location.reload();
          });
        }
      }
    }
  }
});

new Vue({
  el: '#slider',
  data: {
    imgList: [{
      img: 'https://www.w3schools.com/css/trolltunga.jpg',
      header: 'The Motherland Monument',
      desc: 'Kyiv'
    }, {
      img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLrWSYoox3Osb7QBcuJfX1y4Kkq24-bpmdAi__sN5_Cyb2R1Xt',
      header: 'Dyuk Monument',
      desc: 'Odessa'
    }, {
      img: 'http://www.sepeb.com/images/images-008.jpg',
      header: 'Lenin Monument',
      desc: 'Kharkiv'
    }],
    currentImg: 0,
    timer: null
  },
  mounted: function mounted() {
    this.startRotation();
  },

  methods: {
    startRotation: function startRotation() {
      this.timer = setInterval(this.next, 6000);
    },
    stopRotation: function stopRotation() {
      clearTimeout(this.timer);
      this.timer = null;
    },
    next: function next() {
      this.currentImg += 1;
    },
    prev: function prev() {
      this.currentImg -= 1;
    }
  }
});