'use strict';

$(document).ready(function () {
  new SiriWave({
    height: 100,
    speed: 0.05,
    container: document.getElementById('error'),
    autostart: true,
    cover: true,
    color: '#956280'
  });
});