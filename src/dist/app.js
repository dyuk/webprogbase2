'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var base64Img = require('base64-img');
var cities = require('./routes/cities');
var api = require('./routes/api');
var auth = require('./modules/auth');
var cloudinary = require('cloudinary');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var crypto = require('crypto');
var passport = require('passport');
var multer = require('multer');
var Base64 = require('js-base64').Base64;
var ExifImage = require('exif').ExifImage;
var upload = multer({ storage: multer.memoryStorage() });
var LocalStrategy = require('passport-local').Strategy;
mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/CitiesDB', { useMongoClient: true })
mongoose.connect('mongodb://Dyuk:Lthtdjyjvth1@ds251435.mlab.com:51435/heroku_8vczxmsl', { useMongoClient: true }).then(function () {
    return console.log('connection succesful');
}).catch(function (err) {
    return console.error(err);
});
var app = express();
var model = require('./modules/model');
cloudinary.config({
    cloud_name: 'triphelper',
    api_key: '866391184439149',
    api_secret: '3a0_sjSryM_XxSkb6Vs1BNkBrPU'
});

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));
app.use(cookieParser());
app.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use("/cities", cities);
var PORT = process.env.PORT || 8090;

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    model.User.findOne({ _id: id }).then(function (user) {
        if (!user) done("No user");else done(null, user);
    });
});

var serverSalt = "45%sAlT_";
function sha512(password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

passport.use(new LocalStrategy(function (username, password, done) {
    var passwordHash = sha512(password, serverSalt).passwordHash;
    model.User.findOne({ 'username': username, 'passwordHash': passwordHash }).then(function (user) {
        if (!user) return done("No user", false);
        return done(null, user);
    });
}));

app.get('/', async function (req, res) {
    try {
        var data = await model.City.find();
        res.render('index', { data: data, user: req.user });
    } catch (e) {
        res.render('error', {});
    }
});

app.get('/users/profile', auth.checkAuth, async function (req, res) {
    try {
        res.render('profile', { user: req.user });
    } catch (e) {
        res.render('error', {});
    }
});

app.post('/users/updateAvatar', auth.checkAuth, async function (req, res) {
    console.log(req.user.username);
    try {
        var img = req.body.image;
        var url = await cloudinary.uploader.upload(img);
        await model.User.update({ 'username': req.user.username }, {
            image: url.url,
            username: "Admin"
        });
        res.send("OK");
    } catch (e) {
        res.send(null);
    }
});

app.post('/searchMap', auth.checkAuth, async function (req, res) {
    try {
        var tempHistory = req.body;
        console.log(tempHistory);
        tempHistory.city = tempHistory.city.split(",")[0];
        var history = new model.History({
            city: tempHistory.city,
            people: tempHistory.people,
            days: tempHistory.days,
            price: 0,
            user_id: req.user._id,
            lat: tempHistory.lat,
            lng: tempHistory.lng
        });
        history.save().then(function () {
            return res.send("OK");
        }).catch(function (err) {
            res.send(err.toString());
        });
    } catch (e) {
        res.send(e.toString());
    }
});

app.post('/users/postImages', auth.checkAuth, upload.any(), async function (req, res) {
    try {
        for (var i = 0; i < req.files.length; i++) {
            var url = await cloudinary.uploader.upload('data:image/png;base64,' + req.files[i].buffer.toString('base64'));
            new ExifImage({ image: req.files[i].buffer }, function (error, exifData) {
                if (error) console.log('Error: ' + error.message);else console.log(exifData.gps); // Do something with your data!
            });
            var newUrl = url.url;
            var photo = new model.Photo({
                image: newUrl,
                history_id: req.body.history_id
            });
            photo.save();
        }
        res.send("success");
    } catch (e) {
        res.send("error");
    }
});

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

app.get('/admin', auth.checkAdmin, function (req, res) {
    return res.render('admin', {
        user: req.user
    });
});

app.post('/login', passport.authenticate('local'));

app.post('/register', async function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var passwordHash = sha512(password, serverSalt).passwordHash;
    var user = {
        username: username,
        passwordHash: passwordHash,
        role: "user"
    };
    try {
        var userToSave = new model.User(user);
        await userToSave.save();
        res.send("registered");
    } catch (e) {
        res.sendStatus(500);
    }
});

app.get('/search', async function (req, res) {
    try {
        var data = await model.City.find();
        res.send(data);
    } catch (e) {
        res.render('error', {});
    }
});

app.get('/histories', auth.checkAuth, async function (req, res) {
    try {
        var data = await model.History.find({ user_id: req.user._id });
        res.status(200).json(data);
    } catch (e) {
        res.render('error', {});
    }
});

app.get('/photos', auth.checkAuth, async function (req, res) {
    try {
        var id = req.body;
        var data = await model.History.find({ history_id: id });
        res.status(200).json(data);
    } catch (e) {
        res.render('error', {});
    }
});

app.get('/add', auth.checkAdmin, async function (req, res) {
    try {
        var data = await model.City.find();
        res.render('form', { data: data, user: req.user });
    } catch (e) {
        res.render('error', {});
    }
});

app.post('/add', async function (req, res) {
    try {
        var tempObject = req.body;
        tempObject.name = tempObject.name.replace(/\s/g, "_");
        var url = await cloudinary.uploader.upload(tempObject.image);
        tempObject.image = url.url;
        var tempCity = new model.City(tempObject);
        tempCity.save().then(function () {
            return res.send(tempObject);
        }).catch(function (err) {
            res.send(null);
        });
    } catch (e) {
        res.render('error', {});
    }
});

app.use(function (req, res) {
    res.render('error', {});
});

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT);
});