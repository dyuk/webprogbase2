'use strict';

module.exports = {
    checkAdmin: function checkAdmin(req, res, next) {
        if (!req.user) return res.render('error', {});
        if (req.user.role !== 'admin') return res.render('error', {});
        next();
    },
    checkAuth: function checkAuth(req, res, next) {
        if (!req.user) return res.render('error', {});
        next();
    }
};