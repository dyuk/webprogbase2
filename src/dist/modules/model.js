'use strict';

var mongoose = require('mongoose');
var citySchema = mongoose.Schema({
    city: { type: String, unique: true, required: true },
    description: { type: String, required: true },
    name: { type: String, unique: true, required: true },
    image: { type: String, required: true },
    views: Number,
    likes: Number
});

var userSchema = mongoose.Schema({
    username: { type: String, unique: true, required: true },
    passwordHash: { type: String, required: true },
    role: { type: String, required: true },
    image: String
});

var historySchema = mongoose.Schema({
    user_id: mongoose.Schema.Types.ObjectId,
    people: Number,
    days: Number,
    city: String,
    price: Number,
    lat: Number,
    lng: Number
});

var photoSchema = mongoose.Schema({
    history_id: mongoose.Schema.Types.ObjectId,
    image: String
});

var City = mongoose.model('City', citySchema);
var User = mongoose.model('User', userSchema);
var History = mongoose.model('History', historySchema);
var Photo = mongoose.model('Photo', photoSchema);

module.exports = {
    City: City,
    User: User,
    History: History,
    Photo: Photo
};